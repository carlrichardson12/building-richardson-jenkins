import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  // Responsive NavBar Ish
  dropDownClick() {
    const x = document.getElementById('nava');
    if (x.className === 'topnav') {
      x.className += 'responsive';
      let icon = document.getElementById('icon-hamburger');
      icon.style.display = 'hidden'
    } else {
      x.className = 'topnav';
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { Contact } from '../models/Contact';
import { Router } from '@angular/router';
import { ContactService } from '../services/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  public fName: string;
  public  lName: string;
  public  pNumber: number;
  public  email: string;
  public  desc: string;
  public  contactType: boolean;
  public timestamp: Date = new Date();

  constructor(private router: Router, private contactServ: ContactService) { }

  ngOnInit() {
  }
  
  onSubmit() {
    const c: Contact = {
      fName: this.fName,
      lName: this.lName,
      pNumber: this.pNumber,
      email: this.email,
      desc: this. desc,
      contactType: this.contactType,
      timestamp: this.timestamp
    };
    console.log(c);
    this.contactServ.addContact(c).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['/Home']);
      });

  }

}

import { Component, OnInit } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { Admin } from '../models/Admin';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-access',
  templateUrl: './admin-access.component.html',
  styleUrls: ['./admin-access.component.css']
})
export class AdminAccessComponent implements OnInit {
public email: string;
public password: string;
public firstName: string;
public lastName: string;
  newAdmin: boolean = false;
  constructor(private router: Router, private adminServ: AdminService) { }

  ngOnInit() {
  }

  onSubmit() {
const login: any = {
 email: this.email,
 password: this.password
};
console.log(login);
this.adminServ.login(login).subscribe(
  data => {
    console.log(data);
    this.router.navigate(['/Home']);
  });
  }

  onNewSubmit() {
    const newAdmin: any = {
     email: this.email,
     password: this.password,
     firstName: this.firstName,
     lastName: this.lastName
    };
    console.log(newAdmin);
    this.adminServ.newUser(newAdmin).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['/Home']);
      });
      }

      isNewAdmin() {
        if (!this.newAdmin) {
          this.newAdmin = true;
        } else {
          this.newAdmin = false;
        }
        return this.newAdmin;
      }

}

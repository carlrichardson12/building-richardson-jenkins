import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contact } from '../models/Contact';

@Injectable({
    providedIn: 'root'
})

export class ContactService {
    private newURL = 'http://localhost:8009/contact/new';
    private getURL = 'http://localhost:8009/contact/all';

    constructor(private httpCli: HttpClient){ }

    addContact(c: Contact) {
        const eventSuccess = this.httpCli.post<Contact>(this.newURL, c);
        return eventSuccess;
    }

    getContacts(): any {
        const myPeople = this.httpCli.get<Contact[]>(this.getURL);
        return myPeople;
    }
}

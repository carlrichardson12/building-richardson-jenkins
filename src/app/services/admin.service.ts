import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Admin } from '../models/Admin';

@Injectable({
    providedIn: 'root'
})

export class AdminService {
    private loginUrl = 'http://localhost:8009/admin/login';
    private newLoginUrl = 'http://localhost:8009/admin/new';

    constructor(private httpCli: HttpClient){ }

    login(admin: Admin) {
        const loginSuccess = this.httpCli.post<Admin>(this.loginUrl, admin);
        return loginSuccess;
    }

    newUser(admin: Admin) {
        const loginNew = this.httpCli.post<Admin>(this.newLoginUrl, admin);
        return loginNew;
    }

}
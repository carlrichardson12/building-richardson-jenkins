import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Events } from '../models/Events';

@Injectable({
  providedIn: 'root'
})
export class EventEntryService {
  private getURL = 'http://localhost:8009/contact/all';

  constructor(private httpCli: HttpClient) { }
  getContacts(): Observable<Events[]> {
    return this.httpCli.get<Events[]>(this.getURL);
  }
}

import { TestBed } from '@angular/core/testing';

import { EventEntryService } from './event-entry.service';

describe('EventEntryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventEntryService = TestBed.get(EventEntryService);
    expect(service).toBeTruthy();
  });
});

import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contact } from '../models/Contact';
import { ContactService } from '../services/contact.service';

@Injectable({
    providedIn: 'root'
})

class contactParsing {

    

    constructor(private contactService:ContactService){
        let Contact = contactService.getContacts();
    }

    gettingContact(): Contact [] {
        const thePeople: Contact [] = [];
        const contacts = this.contactService.getContacts();

        for(let i=0; i < contacts.length; i++) {
            let newContact = this.gettingInfoOut(contacts[i]);
            thePeople.push(newContact);
        }
        return thePeople;
    }

    gettingInfoOut (contacts:any) {
        const contactPerson: Contact = {
            fName: contacts.fName,
            lName: contacts.lName,
            pNumber:contacts.pNumber,
            email: contacts.email,
            desc: contacts.desc,
            contactType: contacts.contactType,
            timestamp: contacts.timestamp
        }
        return contactPerson;
    }
 }

